#version 450 core

layout (location = 0) in vec3 vPos;
layout (location = 1) in vec4 vCol;

out vec4 fColor;

uniform mat4 MVP;

void main (void) {
    gl_Position = vec4(vPos, 1.0) * MVP;
    fColor = vCol;
}