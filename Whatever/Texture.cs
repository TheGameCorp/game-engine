﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using OpenTK.Graphics.OpenGL;

namespace Whatever {
    public class Texture : IDisposable {
        public int TextureID;

        private static Dictionary<string, Texture> TextureCache = new Dictionary<string, Texture> ();

        public enum TextureFilter {
            Linear,
            Nearest,
        }

        public Texture (string filename) {
            var (rgba, width, height) = filename.GetRGBA ();

            GL.CreateTextures (TextureTarget.Texture2D, 1, out this.TextureID);
            GL.TextureStorage2D (this.TextureID, 6, SizedInternalFormat.Rgba32f, width, height);

            var pix_fmt = PixelFormat.Rgba;

            switch (rgba.Length / (width * height)) {
                case 1:
                    pix_fmt = PixelFormat.Red;
                    break;
                case 2:
                    pix_fmt = PixelFormat.Rg;
                    break;
                case 3:
                    pix_fmt = PixelFormat.Rgb;
                    break;
                case 4:
                    pix_fmt = PixelFormat.Rgba;
                    break;
            }

            GL.TextureSubImage2D (this.TextureID, 0, 0, 0, width, height, pix_fmt, PixelType.UnsignedByte, rgba);

            GL.GetFloat ((GetPName) ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out float max);
            GL.TextureParameter (this.TextureID, (TextureParameterName) ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, max);

            GL.GenerateTextureMipmap (this.TextureID);

            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.LinearMipmapLinear);

            Texture.TextureCache.Add (filename, this);
        }

        public Texture (int width, int height, TextureFilter filter = TextureFilter.Linear) {
            GL.CreateTextures (TextureTarget.Texture2D, 1, out this.TextureID);
            GL.TextureStorage2D (this.TextureID, 4, SizedInternalFormat.Rgba32f, width, height);

            GL.GenerateTextureMipmap (this.TextureID);

            GL.GetFloat ((GetPName) ExtTextureFilterAnisotropic.MaxTextureMaxAnisotropyExt, out float max);
            GL.TextureParameter (this.TextureID, (TextureParameterName) ExtTextureFilterAnisotropic.TextureMaxAnisotropyExt, max);

            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMagFilter, (int) (filter == TextureFilter.Linear ? TextureMagFilter.Linear : TextureMagFilter.Nearest));
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMinFilter, (int) (filter == TextureFilter.Linear ? TextureMinFilter.LinearMipmapLinear : TextureMinFilter.NearestMipmapNearest));
        }

        public Texture (int width, int height, PixelInternalFormat internalFormat, PixelFormat pixFormat, TextureFilter filter) {
            GL.CreateTextures (TextureTarget.Texture2D, 1, out this.TextureID);

            this.Use (() => {
                GL.TexImage2D (TextureTarget.Texture2D, 0, internalFormat, width, height, 0, pixFormat, PixelType.Float, IntPtr.Zero);
            });

            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMagFilter, (int) (filter == TextureFilter.Linear ? TextureMagFilter.Linear : TextureMagFilter.Nearest));
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMinFilter, (int) (filter == TextureFilter.Linear ? TextureMinFilter.Linear : TextureMinFilter.Nearest));
        }

        public void Use (Action action, int id = 0) {
            this.Bind (id);
            action ();
            this.Unbind (id);
        }

        public void Bind (int id = 0) {
            GL.ActiveTexture (TextureUnit.Texture0 + id);
            GL.BindTexture (TextureTarget.Texture2D, this.TextureID);
        }
        public void Unbind (int id = 0) {
            GL.ActiveTexture (TextureUnit.Texture0 + id);
            GL.BindTexture (TextureTarget.Texture2D, 0);
        }

        public void Dispose () => GL.DeleteTexture (this.TextureID);

        public static implicit operator Texture(string filename) => Texture.TextureCache.ContainsKey(filename) ? Texture.TextureCache[filename] : new Texture(filename);

        public static Texture White = "white.png";
        public static Texture Gray = "gray.png";
        public static Texture Black = "black.png";
    }


    [Serializable]
    public class TextureNotFoundException : Exception {
        public TextureNotFoundException () { }
        public TextureNotFoundException (string message) : base (message) { }
        public TextureNotFoundException (string message, Exception inner) : base (message, inner) { }
        protected TextureNotFoundException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }

    public static class TextureExtensions {
        public static (byte[] rgba, int width, int height) GetRGBA (this string @this) {
            var path = Path.Combine ("Assets", "Textures", @this);
            if (!File.Exists (path))
                throw new TextureNotFoundException ($"Could not find texture: {@this}");

            var img = new StbSharp.ImageReader ().Read(File.Open(path, FileMode.Open));
            
            return (img.Data, img.Width, img.Height);
        }
    }
}
