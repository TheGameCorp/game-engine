﻿using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public class PointShader : Shader {
        protected override string ShaderName => "Point";

        public PointShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) {
        }
    }
}
