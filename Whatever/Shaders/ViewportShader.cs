﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public class ViewportShader : Shader {
        protected override string ShaderName => "Viewport";

        public ViewportShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) { }
    }
}
