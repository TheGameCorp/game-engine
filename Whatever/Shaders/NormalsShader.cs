﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public class NormalsShader : Shader {
        protected override string ShaderName => "Normals";

        public NormalsShader () {
            this.Shaders.Add (ShaderType.GeometryShader);
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) {
            this["camera"] = Window.Camera.LookAtMatrix;
            this["model"] = transform.Matrix;
        }
    }
}
