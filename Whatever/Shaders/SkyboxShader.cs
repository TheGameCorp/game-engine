﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public class SkyboxShader : Shader {
        protected override string ShaderName => "Skybox";

        public SkyboxShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) {
            this["uProj"] = Window.Camera.Projection;
            this["uMV"] = Window.Camera.View;
        }
    }
}
