﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    class DepthPassShader : Shader {
        protected override string ShaderName => "DepthPass";

        public DepthPassShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) { }
    }
}
