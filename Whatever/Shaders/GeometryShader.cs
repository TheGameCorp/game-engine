﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public class GeometryShader : Shader {
        protected override string ShaderName => "Geometry";

        public GeometryShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) {
            this["camera"] = Window.Camera.LookAtMatrix;
            this["model"] = transform.Matrix;

            var nrm = transform.Matrix;
            if (nrm.Determinant != 0)
                nrm.Invert ();
            nrm.Transpose ();

            this["normal"] = nrm;
        }

        public void SetUniforms (Transform transform, DirectionalLight light) {
            this["camera"] = light.LightMatrix;
            this["model"] = transform.Matrix;
        }
    }
}
