﻿using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public class LightingShader : Shader {
        protected override string ShaderName => "Lighting";

        public LightingShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) { }

        public void SendBuffers (Texture albedo, Texture normal, Texture metRoughAO, Texture depth) {
            this["tAlbedo"] = albedo;
            this["normal"] = normal;
            this["met_rough_ao"] = metRoughAO;
            this["depth"] = depth;
        }

        public void SendUniforms () {
            this["invProj"] = Window.Camera.LookAtMatrix.Inverted ();
            this["eyePos"] = Window.Camera.Position;
        }
    }
}
