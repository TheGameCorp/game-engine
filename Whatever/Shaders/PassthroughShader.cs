﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    class PassthroughShader : Shader {
        protected override string ShaderName => "Passthrough";

        public PassthroughShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) {
            this["camera"] = Window.Camera.LookAtMatrix;
            this["model"] = transform.Matrix;
        }
    }
}
