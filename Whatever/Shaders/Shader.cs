﻿using System;
using System.Collections.Generic;
using System.IO;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace Whatever.Shaders {
    public abstract class Shader : IDisposable {
        public int ProgramID { get; private set; }

        protected abstract string ShaderName { get; }

        public List<ShaderType> Shaders { get; private set; } = new List<ShaderType> ();

        public Dictionary<string, int> Uniforms { get; private set; } = new Dictionary<string, int> ();

        public abstract void SetUniforms (Transform transform);

        protected void GetLocations () {
            GL.GetProgram (this.ProgramID, GetProgramParameterName.ActiveUniforms, out int unicount);

            for (int i = 0; i < unicount; i++) {
                GL.GetActiveUniformName (this.ProgramID, i, 32, out int length, out string name);
                this.Uniforms[name] = GL.GetUniformLocation (this.ProgramID, name);
            }
        }

        protected void CreateProgram () {
            this.ProgramID = GL.CreateProgram ();

            var shaders = new List<int> ();

            foreach (var shader in this.Shaders) {
                var loc = "";
                switch (shader) {
                    case ShaderType.VertexShader:
                        loc = $"vs{this.ShaderName}.glsl";
                        break;
                    case ShaderType.FragmentShader:
                        loc = $"fs{this.ShaderName}.glsl";
                        break;
                    case ShaderType.GeometryShader:
                        loc = $"gs{this.ShaderName}.glsl";
                        break;
                    case ShaderType.ComputeShader:
                        loc = $"cs{this.ShaderName}.glsl";
                        break;
                    case ShaderType.TessControlShader:
                        loc = $"tc{this.ShaderName}.glsl";
                        break;
                    case ShaderType.TessEvaluationShader:
                        loc = $"te{this.ShaderName}.glsl";
                        break;
                    default:
                        throw new UnsuportedShaderTypeException ($"Unsuported shader type: {Enum.GetName (typeof (ShaderType), shader)}");
                }
                shaders.Add (this.CompileShader (shader, loc));
            }

            foreach (var shader in shaders) {
                GL.AttachShader (this.ProgramID, shader);
            }

            GL.LinkProgram (this.ProgramID);

            var info = GL.GetProgramInfoLog (this.ProgramID);
            if (!string.IsNullOrWhiteSpace (info))
                throw new ShaderProgramLinkException ($"Could not link program {this.ShaderName}:\n{info}");

            foreach (var shader in shaders) {
                GL.DetachShader (this.ProgramID, shader);
                GL.DeleteShader (shader);
            }

            this.GetLocations ();
        }
        protected int CompileShader (ShaderType type, string location) {
            var path = Path.Combine ("Assets", "Shaders", location);
            if (!File.Exists (path))
                throw new ShaderNotFoundException ($"Could not find shader source for: {location}");
            var source = File.ReadAllText (path);

            var shader = GL.CreateShader (type);

            GL.ShaderSource (shader, source);
            GL.CompileShader (shader);

            var info = GL.GetShaderInfoLog (shader);

            if (!string.IsNullOrWhiteSpace (info))
                throw new ShaderCompileException ($"Could not compile shader {location}:\n{info}");

            return shader;
        }

        public object this[string name] {
            set {
                switch (value) {
                    case Matrix4 mat4:
                        GL.UniformMatrix4 (this.Uniforms[name], false, ref mat4);
                        break;
                    case int i:
                        GL.Uniform1 (this.Uniforms[name], i);
                        break;
                    case Vector3 v:
                        GL.Uniform3 (this.Uniforms[name], v);
                        break;
                    case float f:
                        GL.Uniform1 (this.Uniforms[name], f);
                        break;
                    case Transform t:
                        this[name] = t.Matrix;
                        break;
                    case Texture tex:
                        if (this.Uniforms.ContainsKey (name)) {
                            tex.Bind (this.Uniforms[name]);
                            this[name] = this.Uniforms[name];
                        }
                        break;
                    case TextureCubemap cube:
                        if (this.Uniforms.ContainsKey (name)) {
                            cube.Bind (this.Uniforms[name]);
                            this[name] = this.Uniforms[name];
                        }
                        break;
                    case Color4 color:
                        GL.Uniform4 (this.Uniforms[name], color);
                        break;
                    default:
                        throw new UnsuportedUniformException ($"Unsuported uniform type {value.GetType ().Name}");
                }
            }
        }

        public void Use (Action action) {
            this.Bind ();
            action ();
            this.Unbind ();
        }

        public void Bind () => GL.UseProgram (this.ProgramID);
        public void Unbind () => GL.UseProgram (0);

        public void Dispose () => GL.DeleteProgram (this.ProgramID);
    }


    [Serializable]
    public class ShaderNotFoundException : Exception {
        public ShaderNotFoundException () { }
        public ShaderNotFoundException (string message) : base (message) { }
        public ShaderNotFoundException (string message, System.Exception inner) : base (message, inner) { }
        protected ShaderNotFoundException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }


    [Serializable]
    public class ShaderCompileException : Exception {
        public ShaderCompileException () { }
        public ShaderCompileException (string message) : base (message) { }
        public ShaderCompileException (string message, Exception inner) : base (message, inner) { }
        protected ShaderCompileException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }


    [Serializable]
    public class ShaderProgramLinkException : Exception {
        public ShaderProgramLinkException () { }
        public ShaderProgramLinkException (string message) : base (message) { }
        public ShaderProgramLinkException (string message, Exception inner) : base (message, inner) { }
        protected ShaderProgramLinkException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }


    [Serializable]
    public class UnsuportedShaderTypeException : Exception {
        public UnsuportedShaderTypeException () { }
        public UnsuportedShaderTypeException (string message) : base (message) { }
        public UnsuportedShaderTypeException (string message, Exception inner) : base (message, inner) { }
        protected UnsuportedShaderTypeException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }


    [Serializable]
    public class UndefinedUniformException : Exception {
        public UndefinedUniformException () { }
        public UndefinedUniformException (string message) : base (message) { }
        public UndefinedUniformException (string message, Exception inner) : base (message, inner) { }
        protected UndefinedUniformException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }


    [Serializable]
    public class UnsuportedUniformException : Exception {
        public UnsuportedUniformException () { }
        public UnsuportedUniformException (string message) : base (message) { }
        public UnsuportedUniformException (string message, Exception inner) : base (message, inner) { }
        protected UnsuportedUniformException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }
}
