﻿using OpenTK;

namespace Whatever {
    public struct Transform {
        public Vector3 Position;
        public Vector3 Rotation;
        public Vector3 Scale;

        public Matrix4 Matrix => Matrix4.CreateScale (this.Scale) *
                    Matrix4.CreateRotationX (this.Rotation.X) *
                    Matrix4.CreateRotationY (this.Rotation.Y) *
                    Matrix4.CreateRotationZ (this.Rotation.Z) *
                    Matrix4.CreateTranslation (this.Position);

        public Transform (Vector3 pos, Vector3 rot, Vector3 scale) {
            this.Position = pos;
            this.Rotation = rot;
            this.Scale = scale;
        }

        public static Transform Zero => new Transform (Vector3.Zero, Vector3.Zero, Vector3.One);
    }
}
