﻿using OpenTK.Graphics;

namespace Whatever.Lighting {
    public class BaseLight {
        public Color4 Color { get; set; }
    }
}
