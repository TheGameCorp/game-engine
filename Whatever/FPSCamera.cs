﻿using OpenTK;
using OpenTK.Input;

namespace Whatever {
    public class FPSCamera : ICamera {
        public Matrix4 Projection { get; set; }
        public Matrix4 LookAtMatrix => Matrix4.CreateTranslation (-this.Position) * this.Orientation * this.Projection;
        public Matrix4 View => Matrix4.CreateTranslation (-this.Position) * this.Orientation;
        public Vector3 Position { get; set; }
        public Vector3 Rotation => this.rotation;
        public Matrix4 Orientation => Matrix4.CreateRotationY (this.Rotation.X) * Matrix4.CreateRotationX (this.Rotation.Y);

        private Vector3 rotation;

        private Vector3 oldPos = Vector3.Zero;

        public bool Moved => this.moved;

        private bool moved = false;

        public bool FreeCam { get; set; } = false;
        public float XSens { get; set; } = 0.1f;
        public float YSens { get; set; } = 0.1f;
        

        const float deg90 = 1.5708f;
        const float deg360 = 6.28319f;

        private const float V = 1.4f * 2;

        public float AspectRatio {
            set => this.Projection = Matrix4.CreatePerspectiveFieldOfView (MathHelper.DegreesToRadians (60), value, 0.1f, 256f);
        }

        public void Translate (Vector3 pos) { }
        public void Update (KeyboardState keys, float dt) {
            var movement = Vector3.Zero;

            if (keys.IsKeyDown (Key.W))
                movement.Z -= V * dt;
            if (keys.IsKeyDown (Key.S))
                movement.Z += V * dt;

            if (keys.IsKeyDown (Key.A))
                movement.X -= V * dt;
            if (keys.IsKeyDown (Key.D))
                movement.X += V * dt;

            Vector3 trans;

            if (this.FreeCam)
                trans = Vector4.Transform (new Vector4 (movement, 1.0f), this.Orientation.Inverted ()).Xyz;
            else
                trans = Vector3.Transform (movement, Matrix3.CreateRotationY (this.Rotation.X).Inverted ());

            if (keys.IsKeyDown (Key.Space))
                trans.Y += V * dt;
            if (keys.IsKeyDown (Key.LShift))
                trans.Y -= V * dt;

            this.Position += trans;

            if (this.Position != this.oldPos)
                this.moved = true;
            else
                this.moved = false;

            this.oldPos = this.Position;
        }
        public void UpdateAim (float dx, float dy, float dt) {
            this.rotation.X += dx * dt * this.XSens;
            this.rotation.Y += dy * dt * this.YSens;

            this.clampAim ();
        }

        private void clampAim () {
            this.rotation.Y = MathHelper.Clamp (this.rotation.Y, -deg90, deg90);
            
            if (this.rotation.X >= deg360)
                this.rotation.X -= deg360;
            if (this.rotation.X <= -deg360)
                this.rotation.X += deg360;
        }
    }
}
