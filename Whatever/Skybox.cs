﻿using Whatever.Shaders;

namespace Whatever {
    public class Skybox {
        public TextureCubemap Cubemap;
        public Geometry Geom;

        public SkyboxShader Shader;

        public Skybox (TextureCubemap cubemap) {
            this.Cubemap = cubemap;
            this.Geom = ObjectFactory.CreateScreenGeometry ();
            this.Shader = new SkyboxShader ();
        }

        public void Render () => this.Shader.Use (() => {
            this.Shader["uTex"] = 0;
            this.Shader.SetUniforms (Transform.Zero);
            this.Cubemap.Use (() => {
                this.Geom.RenderCall ();
            });
        });
    }
}
