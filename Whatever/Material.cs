﻿using System;
using Whatever.Shaders;

namespace Whatever {
    public class Material {
        public Texture Diffuse { get; set; }
        public Texture Normal { get; set; }
        public Texture Ambient { get; set; }
        public Texture Roughness { get; set; }
        public Texture Metal { get; set; }

        public Material (Texture diffuse, Texture normal) : this (diffuse, normal, Texture.White, Texture.Gray, Texture.Black) { }
        public Material (Texture diffuse, Texture normal, Texture ambient, Texture roughness, Texture metal) {
            this.Diffuse = diffuse;
            this.Normal = normal;
            this.Ambient = ambient;
            this.Roughness = roughness;
            this.Metal = metal;
        }

        public static implicit operator Material ((Texture diffuse, Texture normal) val) => new Material(val.diffuse, val.normal);

        public static implicit operator Material ((Texture diffuse, Texture normal, Texture ambient, Texture roughness) val) => new Material (val.diffuse, val.normal, val.ambient, val.roughness, Texture.Black);
        public static implicit operator Material ((Texture diffuse, Texture normal, Texture ambient, Texture roughness, Texture metal) val) => new Material (val.diffuse, val.normal, val.ambient, val.roughness, val.metal);

        public void Use (Shader shader, Action action) {
            shader["tDiffuse"] = this.Diffuse;
            shader["tAmbient"] = this.Ambient;
            shader["tRoughness"] = this.Roughness;
            shader["tNormal"] = this.Normal;
            shader["tMetal"] = this.Metal;
            action ();
        }
    }
}
