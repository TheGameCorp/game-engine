﻿using System.Collections.Generic;
using OpenTK;
using Whatever.Shaders;

namespace Whatever {
    public class Scene {
        public SceneObject Root { get; set; }

        public Scene () => this.Root = new SceneObject ();

        public static Scene operator + (Scene @this, SceneObject other) {
            @this.Root += other;
            return @this;
        }

        public void Update (float dt) {
            this.Root.Update (dt);
            if (this.Root.Children != null)
                this.Root.Children.ForEach ((SceneObject obj) => {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.Update (dt);
                            break;
                    }
                });
        }
    }

    public class SceneObject {
        public Transform WorldTransform;
        public Transform LocalTransform;
        public SceneObject Parent;
        public List<SceneObject> Children { get; set; } = new List<SceneObject> ();

        public SceneObject (Transform transform) => this.WorldTransform = this.LocalTransform = transform;

        public SceneObject () => this.WorldTransform = this.LocalTransform = Transform.Zero;

        public static SceneObject operator + (SceneObject @this, SceneObject other) {
            if (@this.Children == null)
                @this.Children = new List<SceneObject> ();
            other.Parent = @this;
            @this.Children.Add (other);
            return @this;
        }

        public void AddChild (SceneObject other) {
            if (this.Children == null)
                this.Children = new List<SceneObject> ();
            other.Parent = this;
            this.Children.Add (other);
        }

        public SceneObject Translated (Vector3 pos) {
            this.WorldTransform.Position += pos;
            return this;
        }

        public SceneObject Rotated (Vector3 rot) {
            this.WorldTransform.Rotation += new Vector3 (MathHelper.DegreesToRadians (rot.X), MathHelper.DegreesToRadians (rot.Y), MathHelper.DegreesToRadians (rot.Z));
            return this;
        }

        public SceneObject Scaled (Vector3 scale) {
            this.WorldTransform.Scale += scale;
            return this;
        }

        public virtual void Update (float dt) {
        }

        public static implicit operator SceneObject ((Geometry geom, Material mat) obj) => new RenderableObject (obj.geom, obj.mat);
    }

    public class RenderableObject : SceneObject {
        public Geometry Geometry { get; set; }
        public Material Material { get; set; }

        public RenderableObject (Geometry geom, Material mat) {
            this.Geometry = geom;
            this.Material = mat;
        }

        public RenderableObject () { }

        public void Render (Shader shader) {
            this.Geometry.Render (shader, this.Material, this.WorldTransform);
            if (this.Children != null)
                this.Children.ForEach((SceneObject obj) => {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.Render (shader);
                            break;
                    }
                });
        }

        public void Render (GeometryShader shader, BaseLight light) {
            this.Geometry.Render (light, shader, this.Material, this.WorldTransform);
            if (this.Children != null)
                this.Children.ForEach ((SceneObject obj) => {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.Render (shader, light);
                            break;
                    }
                });
        }

        public void RenderDebug (Shader shader) {
            this.Geometry.RenderDebug (shader, this.WorldTransform);
            if (this.Children != null)
                this.Children.ForEach ((SceneObject obj) => {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.RenderDebug (shader);
                            break;
                    }
                });
        }

        public static implicit operator RenderableObject ((Geometry geom, Material mat) obj) => new RenderableObject (obj.geom, obj.mat);
    }
}
