﻿using System.IO;
using Newtonsoft.Json;

namespace Whatever {
    public class Config {
        public static JsonSerializer Serializer = new JsonSerializer ();
        public static string Location = Path.Combine ("Assets", "Config");

        public static void WriteConfig (object config, string location) {
            var path = Path.Combine (Config.Location, $"{location}.json");

            if (!Directory.Exists (Config.Location))
                Directory.CreateDirectory (Config.Location);

            using (StreamWriter file = File.CreateText (path))
                Config.Serializer.Serialize (file, config);
        }

        public static T LoadConfig<T> (string location) {
            var path = Path.Combine (Config.Location, $"{location}.json");
            if (!File.Exists (path))
                throw new ConfigNotFoundException ($"Could not find configuration file: ${location}");

            using (StreamReader file = File.OpenText (path))
                return (T) Config.Serializer.Deserialize (file, typeof (T));
        }
    }


    [System.Serializable]
    public class ConfigNotFoundException : System.Exception {
        public ConfigNotFoundException () { }
        public ConfigNotFoundException (string message) : base (message) { }
        public ConfigNotFoundException (string message, System.Exception inner) : base (message, inner) { }
        protected ConfigNotFoundException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }
}
