﻿using System;
using OpenTK;
using OpenTK.Graphics;
using Whatever.Shaders;

namespace Whatever {
    public abstract class BaseLight {
        public Color4 Color { get; set; }
        public Vector3 Position { get; set; }
        public float Intensity { get; set; }

        public abstract void Use (Renderer renderer, Scene scene, Shader shader, Action action);
        public abstract void SendUniforms (Shader shader);
    }
}
