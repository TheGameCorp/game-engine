﻿using System;
using System.Collections.Generic;
using Whatever.Shaders;
using OpenTK.Graphics.OpenGL;

namespace Whatever {
    public class Renderer {
        public GBuffer RenderTarget { get; set; }

        public Shader Passthrough, Debug, Geometry, Depth;

        public LightingShader Lighting;

        private Geometry screen;

        public List<BaseLight> Lights { get; set; }

        private (int Width, int Height) size;
        public (int Width, int Height) Size {
            get => this.size;
            set {
                this.size = value;
                this.RenderTarget = new GBuffer (this.size.Width, this.size.Height);
            }
        }

        public Renderer ((int width, int height) size, Shader geom, LightingShader lighting, Shader pass, Shader debug, Shader depth) {
            this.RenderTarget = new GBuffer (size.width, size.height);
            this.Passthrough = pass;
            this.Debug = debug;
            this.Geometry = geom;
            this.Lighting = lighting;
            this.Depth = depth;
            this.screen = ObjectFactory.CreateScreenGeometry ();

            this.Lights = new List<BaseLight> ();
        }

        public void Render (Scene scene) =>
            this.Passthrough.Use (() => {
                foreach (SceneObject obj in scene.Root.Children) {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.Render (this.Passthrough);
                            break;
                    }
                }
            });

        public void RenderDebug (Scene scene) =>
            this.Debug.Use (() => {
                foreach (SceneObject obj in scene.Root.Children) {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.RenderDebug (this.Debug);
                            break;
                    }
                }
            });

        public void Defer (Scene scene) =>
            this.Geometry.Use (() => {
                foreach (SceneObject obj in scene.Root.Children) {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.Render (this.Geometry);
                            break;
                    }
                }
            });

        public void Defer (Scene scene, BaseLight light) =>
            this.Geometry.Use (() => {
                foreach (SceneObject obj in scene.Root.Children) {
                    switch (obj) {
                        case RenderableObject robj:
                            robj.Render ((GeometryShader) this.Geometry, light);
                            break;
                    }
                }
            });

        public void DepthPrepass () =>
            this.Depth.Use (() => {
                this.Depth["tAlbedo"] = this.RenderTarget.Color;
                this.screen.RenderCall ();
            });

        public void Light (Scene scene) {
            foreach (var light in this.Lights)
                light.Use (this, scene, this.Lighting, () => {
                    this.Lighting.Use (() => {
                        GL.BlendFunc (BlendingFactor.One, BlendingFactor.One);
                        GL.DepthFunc (DepthFunction.Lequal);
                        this.Lighting.SendBuffers (this.RenderTarget.Color, this.RenderTarget.Normal, this.RenderTarget.MetRoughAO, this.RenderTarget.Depth);
                        this.Lighting.SendUniforms ();
                        light.SendUniforms (this.Lighting);
                        this.screen.RenderCall ();
                    });
                });
        }

        public void Use (Action action) {
            this.RenderTarget.Begin ();
            this.RenderTarget.Use (() => {
                action ();
            });
            this.RenderTarget.End ();
        }
    }
}
