﻿using System;
using System.Collections.Generic;
using System.IO;
using Assimp;
using OpenTK;
using OpenTK.Graphics;

namespace Whatever {
    public class AssimpLoader {
        public static (Vertex[], List<uint[]>) LoadModel (string location) {
            var path = Path.Combine ("Assets", "Models", location);
            if (!File.Exists (path))
                throw new ModelNotFoundException ($"Could not find model: {location}");

            var importer = new AssimpContext ();
            var scene = importer.ImportFile (path, PostProcessPreset.TargetRealTimeMaximumQuality | PostProcessSteps.FlipUVs);

            foreach (var tex in scene.Materials) {
                Console.WriteLine ($"{location} : {tex.TextureDiffuse.FilePath}");
            }

            return scene.ToGeometry ();
        }
    }


    [Serializable]
    public class ModelNotFoundException : Exception {
        public ModelNotFoundException () { }
        public ModelNotFoundException (string message) : base (message) { }
        public ModelNotFoundException (string message, Exception inner) : base (message, inner) { }
        protected ModelNotFoundException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }

    public static class Extensions {
        public static Vector3 ToVector3 (this Vector3D val) => new Vector3 (val.X, val.Y, val.Z);
        public static Vector2 ToVector2 (this Vector3D val) => new Vector2 (val.X, val.Y);
        public static Vector2 ToVector2 (this Vector2D val) => new Vector2 (val.X, val.Y);

        public static List<Vertex> ToVertexArray (this Mesh @this) {
            var verts = new List<Vertex> ();
            for (int i = 0; i < @this.Vertices.Count; i++) {
                Vector3 pos = @this.Vertices[i].ToVector3 ();
                Vector2 tex = @this.HasTextureCoords (0) ? @this.TextureCoordinateChannels[0][i].ToVector2 () : Vector2.Zero;
                Vector3 norm = @this.Normals[i].ToVector3 ();
                Vector3 tan = @this.Tangents.Count != 0 ? @this.Tangents[i].ToVector3 () : Vector3.Zero;
                verts.Add (new Vertex (pos, Color4.White, tex, norm, tan));
            }

            return verts;
        }

        public static (uint[] indices, uint offset) ToIndexArray (this (Mesh mesh, uint offset) @this) {
            var inds = new List<uint> ();

            foreach (var group in @this.mesh.Faces) {
                foreach (var ind in group.Indices) {
                    inds.Add ((uint) ind + @this.offset);
                }
            }

            return (inds.ToArray (), @this.offset + (uint) @this.mesh.Vertices.Count);
        }

        public static (Vertex[] vertices, List<uint[]> indices) ToGeometry (this Assimp.Scene @this) {
            var vertices = new List<Vertex> ();
            var indices = new List<uint[]> ();

            uint indexOffset = 0;

            foreach (var mesh in @this.Meshes) {
                vertices.AddRange (mesh.ToVertexArray ());
                var inds = (mesh, indexOffset).ToIndexArray ();

                indices.Add (inds.indices);
                indexOffset = inds.offset;
            }

            return (vertices.ToArray (), indices);
        }
    }
}
