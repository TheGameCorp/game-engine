﻿using System;
using OpenTK.Graphics.OpenGL4;

namespace Whatever {
    public class TextureCubemap {
        public int TextureID;

        public TextureCubemap ((string up, string down, string front, string back, string left, string right) textures) {
            var up = textures.up.GetRGBA ();
            var down = textures.down.GetRGBA ();
            var front = textures.front.GetRGBA ();
            var back = textures.back.GetRGBA ();
            var left = textures.left.GetRGBA ();
            var right = textures.right.GetRGBA ();
            
            GL.CreateTextures (TextureTarget.TextureCubeMap, 1, out this.TextureID);
            GL.BindTexture (TextureTarget.TextureCubeMap, this.TextureID);
            GL.TexStorage2D (TextureTarget2d.TextureCubeMap, 1, SizedInternalFormat.Rgba32f, up.width, up.height);

            GL.TexSubImage2D (TextureTarget.TextureCubeMapPositiveY, 0, 0, 0, up.width, up.height, up.rgba.Length / (up.width * up.height) == 4 ? PixelFormat.Rgba : PixelFormat.Rgb, PixelType.UnsignedByte, up.rgba);
            GL.TexSubImage2D (TextureTarget.TextureCubeMapNegativeY, 0, 0, 0, down.width, down.height, down.rgba.Length / (down.width * down.height) == 4 ? PixelFormat.Rgba : PixelFormat.Rgb, PixelType.UnsignedByte, down.rgba);
            GL.TexSubImage2D (TextureTarget.TextureCubeMapPositiveZ, 0, 0, 0, front.width, front.height, front.rgba.Length / (front.width * front.height) == 4 ? PixelFormat.Rgba : PixelFormat.Rgb, PixelType.UnsignedByte, front.rgba);
            GL.TexSubImage2D (TextureTarget.TextureCubeMapNegativeZ, 0, 0, 0, back.width, back.height, back.rgba.Length / (back.width * back.height) == 4 ? PixelFormat.Rgba : PixelFormat.Rgb, PixelType.UnsignedByte, back.rgba);
            GL.TexSubImage2D (TextureTarget.TextureCubeMapPositiveX, 0, 0, 0, left.width, left.height, left.rgba.Length / (left.width * left.height) == 4 ? PixelFormat.Rgba : PixelFormat.Rgb, PixelType.UnsignedByte, left.rgba);
            GL.TexSubImage2D (TextureTarget.TextureCubeMapNegativeX, 0, 0, 0, right.width, right.height, right.rgba.Length / (right.width * right.height) == 4 ? PixelFormat.Rgba : PixelFormat.Rgb, PixelType.UnsignedByte, right.rgba);

            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMagFilter, (int) TextureMagFilter.Linear);
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureMinFilter, (int) TextureMinFilter.Linear);
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureWrapS, (int) TextureWrapMode.ClampToEdge);
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureWrapT, (int) TextureWrapMode.ClampToEdge);
            GL.TextureParameter (this.TextureID, TextureParameterName.TextureWrapR, (int) TextureWrapMode.ClampToEdge);
        }

        public void Use (Action action, int id = 0) {
            this.Bind (id);
            action ();
            this.Unbind (id);
        }

        public void Bind (int id = 0) {
            GL.ActiveTexture (TextureUnit.Texture0 + id);
            GL.BindTexture (TextureTarget.TextureCubeMap, this.TextureID);
        }
        public void Unbind (int id = 0) {
            GL.ActiveTexture (TextureUnit.Texture0 + id);
            GL.BindTexture (TextureTarget.TextureCubeMap, 0);
        }

        public void Dispose () => GL.DeleteTexture (this.TextureID);
    }
}
