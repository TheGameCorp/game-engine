﻿using OpenTK;
using OpenTK.Graphics;

namespace Whatever {
    public class ObjectFactory {
        public static (Vertex[], uint[]) CreateSolidCube (float side, Color4 color) {
            side = side / 2f;

            return (new Vertex[] {
               new Vertex (new Vector3 (-side, -side, -side), color, new Vector2(0, 0), new Vector3(-1, -1, -1), new Vector3(-1, -1, -1)), // 0
               new Vertex (new Vector3 (-side, -side, +side), color, new Vector2(0, 0), new Vector3(-1, -1, +1), new Vector3(-1, -1, -1)), // 1
               new Vertex (new Vector3 (-side, +side, -side), color, new Vector2(0, 1), new Vector3(-1, +1, -1), new Vector3(-1, -1, -1)), // 2
               new Vertex (new Vector3 (-side, +side, +side), color, new Vector2(0, 1), new Vector3(-1, +1, +1), new Vector3(-1, -1, -1)), // 3
               new Vertex (new Vector3 (+side, -side, -side), color, new Vector2(1, 0), new Vector3(+1, -1, -1), new Vector3(-1, -1, -1)), // 4
               new Vertex (new Vector3 (+side, +side, -side), color, new Vector2(1, 1), new Vector3(+1, +1, -1), new Vector3(-1, -1, -1)), // 5
               new Vertex (new Vector3 (+side, -side, +side), color, new Vector2(1, 0), new Vector3(+1, -1, +1), new Vector3(-1, -1, -1)), // 6
               new Vertex (new Vector3 (+side, +side, +side), color, new Vector2(1, 1), new Vector3(+1, +1, +1), new Vector3(-1, -1, -1)), // 7
            }, new uint[] {
                0, 1, 2,
                2, 1, 3,

                4, 5, 6,
                6, 5, 7,

                0, 4, 1,
                1, 4, 6,

                2, 3, 5,
                5, 3, 7,

                0, 2, 4,
                4, 2, 5,

                1, 6, 3,
                3, 6, 7,
            });
        }

        public static (Vertex[], uint[]) CreateScreenGeometry () => (new Vertex[] {
                new Vertex (new Vector3 (-1, -1, 0), Color4.White, new Vector2 (0), new Vector3 (0), new Vector3(0)),
                new Vertex (new Vector3 (1, -1, 0), Color4.White, new Vector2 (0), new Vector3 (0), new Vector3(0)),
                new Vertex (new Vector3 (1, 1, 0), Color4.White, new Vector2 (0), new Vector3 (0), new Vector3(0)),
                new Vertex (new Vector3 (-1, 1, 0), Color4.White, new Vector2 (0), new Vector3 (0), new Vector3(0)),
            },
            new uint[] {
                0, 1, 2,
                2, 3, 0,
            });

    }
}
