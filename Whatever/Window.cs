﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using OpenTK.Input;
using Whatever.Shaders;
using Whatever.Terrain;

namespace Whatever {
    public sealed class Window : GameWindow {
        public static int DRAWCALLS = 0;
        
        Scene scene;
        Renderer renderer;
        Skybox skybox;

        public static TextureCubemap EnvironmentTexture;

        public static ICamera Camera = new FPSCamera ();

        public Window() :
            base (1280, 720, GraphicsMode.Default, "Whatever", GameWindowFlags.Default, DisplayDevice.Default, 4, 3, GraphicsContextFlags.ForwardCompatible) => this.Title += ": OpenGL Version: " + GL.GetString (StringName.Version);

        protected override void OnResize(EventArgs e) {
            GL.Viewport (0, 0, this.Width, this.Height);
            Window.Camera.AspectRatio = (float) this.Width / (float) this.Height;
            this.renderer.Size = (this.Width, this.Height);
        }
        protected override void OnLoad(EventArgs e) {
            this.CursorVisible = true;

            if (Environment.OSVersion.Platform == PlatformID.Unix)
                Assimp.Unmanaged.AssimpLibrary.Instance.LoadLibrary ("libassimp.so.4");

            this.scene = new Scene ();
            this.renderer = new Renderer ((this.Width, this.Height), new GeometryShader (), new LightingShader (), new PassthroughShader (), new NormalsShader (), new DepthPassShader ());

            //this.scene += new RenderableObject ("Crate1.obj", ("crate0_diffuse.png", "crate0_normal.png")).Translated (new Vector3 (0, 0, -5));
            this.scene += ("dragon.obj", ("gold/gold-scuffed_basecolor.png", "gold/gold-scuffed_normal.png", Texture.White, "gold/gold-scuffed_roughness.png", "gold/gold-scuffed_metallic.png"));

            //this.scene += new RenderableObject ("test tv.obj", ("Frame_diffuse.png", "crate0_normal.png")).Translated (new Vector3 (0, 1, -4.2f));

            //this.scene += new RenderableObject ("3ds-test.obj", ("crate0_diffuse.png", "crate0_normal.png")).Translated (new Vector3 (0, -6, -5)).Rotated(new Vector3(0, 180, 0)).Scaled(new Vector3(1.5f));

            //this.scene += new RenderableObject ("bus-stop.obj", ("crate0_diffuse.png", "crate0_normal.png")).Translated (new Vector3 (0, 0, 5));

            this.scene += new RenderableObject ("mitsuba.obj", ("oakfloor/oakfloor_basecolor.png", "oakfloor/oakfloor_normal.png", "oakfloor/oakfloor_AO.png", "oakfloor/oakfloor_roughness.png")).Translated (new Vector3 (0, 0, -5));
            this.scene += new RenderableObject ("mitsuba.obj", ("rustediron/rustediron2_basecolor.png", "rustediron/rustediron2_normal.png", Texture.White, "rustediron/rustediron2_roughness.png", "rustediron/rustediron2_metallic.png")).Translated (new Vector3 (-3, 0, -5));
            this.scene += new RenderableObject ("mitsuba.obj", ("bathroomtile/bathroomtile2-basecolor.png", "bathroomtile/bathroomtile2-normal-ogl.png", "bathroomtile/bathroomtile2-ao.png", Texture.Gray)).Translated (new Vector3 (3, 0, -5));
            this.scene += new RenderableObject ("mitsuba.obj", ("gold/gold-scuffed_basecolor.png", "gold/gold-scuffed_normal.png", Texture.White, "gold/gold-scuffed_roughness.png", "gold/gold-scuffed_metallic.png")).Translated (new Vector3 (6, 0, -5));

            this.scene += new RenderableObject ("mitsuba.obj", ("oakfloor/oakfloor_basecolor.png", "oakfloor/oakfloor_normal.png", "oakfloor/oakfloor_AO.png", "oakfloor/oakfloor_roughness.png")).Translated (new Vector3 (0, 0, 5)).Rotated (new Vector3 (0, 180, 0));
            this.scene += new RenderableObject ("mitsuba.obj", ("rustediron/rustediron2_basecolor.png", "rustediron/rustediron2_normal.png", Texture.White, "rustediron/rustediron2_roughness.png", "rustediron/rustediron2_metallic.png")).Translated (new Vector3 (-3, 0, 5)).Rotated (new Vector3 (0, 180, 0));
            this.scene += new RenderableObject ("mitsuba.obj", ("bathroomtile/bathroomtile2-basecolor.png", "bathroomtile/bathroomtile2-normal-ogl.png", "bathroomtile/bathroomtile2-ao.png", Texture.Gray)).Translated (new Vector3 (3, 0, 5)).Rotated (new Vector3 (0, 180, 0));
            this.scene += new RenderableObject ("mitsuba.obj", ("gold/gold-scuffed_basecolor.png", "gold/gold-scuffed_normal.png", Texture.White, "gold/gold-scuffed_roughness.png", "gold/gold-scuffed_metallic.png")).Translated (new Vector3 (6, 0, 5)).Rotated (new Vector3 (0, 180, 0));

            this.scene += new RenderableObject ("ground.dae", ("grass/grass1-albedo3.png", "grass/grass1-normal2.png", "grass/grass1-ao.png", Texture.White)).Rotated (new Vector3 (90, 0, 180)).Scaled (new Vector3 (10));

            Window.EnvironmentTexture = new TextureCubemap
                               (("TropicalSunnyDayUp2048.png", "TropicalSunnyDayDown2048.png",
                                 "TropicalSunnyDayFront2048.png", "TropicalSunnyDayBack2048.png",
                                 "TropicalSunnyDayLeft2048.png", "TropicalSunnyDayRight2048.png"));

            this.skybox = new Skybox (Window.EnvironmentTexture);

            //this.renderer.Lights.Add (new DirectionalLight (new Vector3 (2, 2, 2), new Color4 (0.91f, 0.83f, 0.80f, 1.0f), 1f));
            this.renderer.Lights.Add (new DirectionalLight (new Vector3 (10, 10, 10), new Color4 (1f, 0f, 0f, 1.0f), 0.75f));
            //this.renderer.Lights.Add (new DirectionalLight (new Vector3 (10, 10, -10), new Color4 (0f, 1f, 0f, 1.0f), 0.75f));
            this.renderer.Lights.Add (new DirectionalLight (new Vector3 (-5, 10, 0), new Color4 (0f, 0f, 1f, 1.0f), 0.75f));

            GC.Collect ();

            GL.Enable (EnableCap.Blend);
            GL.Enable (EnableCap.DepthTest);
            GL.Enable (EnableCap.CullFace);
            GL.Enable (EnableCap.Texture2D);
            GL.Enable (EnableCap.TextureCubeMapSeamless);
            GL.CullFace (CullFaceMode.Back);

            GL.PolygonMode (MaterialFace.Front, PolygonMode.Fill);
            GL.PatchParameter (PatchParameterInt.PatchVertices, 3);
            GL.LineWidth (2f);
        }
        protected override void OnUpdateFrame (FrameEventArgs e) {
            this.handleKeyboard ((float) e.Time);

            this.scene.Update ((float) e.Time);
        }

        //GL.BlitNamedFramebuffer (this.renderer.RenderTarget.FramebufferID, 0, 0, 0, this.Width, this.Height, 0, 0, this.Width, this.Height, ClearBufferMask.ColorBufferBit, BlitFramebufferFilter.Nearest);

        protected override void OnRenderFrame(FrameEventArgs e) {
            this.Title = $"(VSync: {this.VSync}) FPS: {1f / e.Time:0} Drawcalls: {Window.DRAWCALLS} Camera: {Window.Camera.Rotation}";
            Window.DRAWCALLS = 0;

            GL.ClearColor (Color4.CornflowerBlue);
            GL.Clear (ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

            GL.BlendFunc (BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);

            GL.PolygonMode (MaterialFace.Front, PolygonMode.Fill);
            this.skybox.Render ();
            GL.Clear (ClearBufferMask.DepthBufferBit);

            this.renderer.Use (() => {
                if (this.wireframe)
                    GL.PolygonMode (MaterialFace.Front, PolygonMode.Line);
                else
                    GL.PolygonMode (MaterialFace.Front, PolygonMode.Fill);

                this.renderer.Defer (this.scene);

                if (this.debug)
                    this.renderer.RenderDebug (this.scene);
            });

            GL.BlendFunc (BlendingFactor.SrcAlpha, BlendingFactor.OneMinusSrcAlpha);

            GL.PolygonMode (MaterialFace.Front, PolygonMode.Fill);
            this.renderer.DepthPrepass ();
            this.renderer.Light (this.scene);

            this.SwapBuffers ();
        }

        float[] clear = new float[] { 0, 0, 0, 1 };

        KeyboardState oldkb = Keyboard.GetState();
        MouseState oldmouse = Mouse.GetState ();

        bool lockMouse = false;
        bool fs = false;
        bool wireframe = false;
        bool debug = false;

        bool fullscreen {
            get {
                this.fs = !this.fs;
                return this.fs;
            }
        }

        private void handleKeyboard (float dt) {
            var keyState = Keyboard.GetState ();
            var mouse = Mouse.GetState ();

            if (keyState.IsKeyDown (Key.Escape))
                this.Exit ();

            if (keyState.IsKeyDown (Key.M) && this.oldkb.IsKeyUp (Key.M))
                this.lockMouse = !this.lockMouse;
            if (keyState.IsKeyDown (Key.N) && this.oldkb.IsKeyUp (Key.N))
                ((FPSCamera) Window.Camera).FreeCam = !((FPSCamera) Window.Camera).FreeCam;
            if (keyState.IsKeyDown (Key.F) && this.oldkb.IsKeyUp (Key.F))
                base.WindowState = this.fullscreen ? WindowState.Fullscreen : WindowState.Normal;

            Window.Camera.Update (keyState, dt);
            if (mouse.IsButtonDown(MouseButton.Left) & !this.lockMouse)
                Window.Camera.UpdateAim (-(mouse.X - this.oldmouse.X), -(mouse.Y - this.oldmouse.Y), dt);
            else if (mouse.IsButtonUp (MouseButton.Left) & this.lockMouse)
                Window.Camera.UpdateAim (mouse.X - this.oldmouse.X, mouse.Y - this.oldmouse.Y, dt);

            if (this.lockMouse) {
                Mouse.SetPosition (this.Width / 2, this.Height / 2);
                this.CursorVisible = false;
            } else {
                this.CursorVisible = true;
            }

            if (keyState.IsKeyDown (Key.E) && this.oldkb.IsKeyUp (Key.E))
                this.wireframe = !this.wireframe;
            if (keyState.IsKeyDown (Key.N) && this.oldkb.IsKeyUp (Key.N))
                this.debug = !this.debug;

            this.oldmouse = mouse;
            this.oldkb = keyState;
        }

        protected override void OnClosed(EventArgs e) => this.Exit ();

        public override void Exit() {
            base.Exit ();
            this.CursorVisible = true;
        }
    }
}
