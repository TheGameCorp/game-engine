﻿using System;
using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;
using Whatever.Shaders;

namespace Whatever {
    public class DirectionalLight : BaseLight {
        public int DepthFBO = -1;
        Texture Depth;

        public Matrix4 LightMatrix => this.LightProjection * this.LightView;
        public Matrix4 LightView => Matrix4.LookAt (this.Position, -this.Position, Vector3.UnitY);
        public Matrix4 LightProjection => Matrix4.CreateOrthographicOffCenter (-10, 10, -10, 10, 1f, 7.5f);

        public DirectionalLight (Vector3 position, Color4 color, float intensity) {
            this.Position = position;
            this.Color = color;
            this.Intensity = intensity;

            GL.CreateFramebuffers (1, out this.DepthFBO);
            this.Depth = new Texture (4096, 4096, PixelInternalFormat.DepthComponent, PixelFormat.DepthComponent, Texture.TextureFilter.Linear);

            GL.TextureParameter (this.Depth.TextureID, TextureParameterName.TextureCompareMode, (int) TextureCompareMode.CompareRefToTexture);
            GL.TextureParameter (this.Depth.TextureID, TextureParameterName.TextureCompareFunc, (int) DepthFunction.Lequal);

            GL.NamedFramebufferTexture (this.DepthFBO, FramebufferAttachment.DepthAttachment, this.Depth.TextureID, 0);

            GL.NamedFramebufferDrawBuffer (this.DepthFBO, DrawBufferMode.None);
            GL.NamedFramebufferReadBuffer (this.DepthFBO, ReadBufferMode.None);
        }

        public override void Use (Renderer renderer, Scene scene, Shader shader, Action action) {
            GL.Viewport (0, 0, 4096, 4096);
            GL.BindFramebuffer (FramebufferTarget.DrawFramebuffer, this.DepthFBO);
            GL.Clear (ClearBufferMask.DepthBufferBit);
            renderer.Defer (scene, this);
            GL.BindFramebuffer (FramebufferTarget.DrawFramebuffer, 0);
            GL.Viewport (0, 0, renderer.Size.Width, renderer.Size.Height);
            GL.DrawBuffer (DrawBufferMode.Back);

            action ();
        }

        public override void SendUniforms (Shader shader) {
            shader["light.position"] = this.Position;
            shader["light.color"] = this.Color;
            shader["light.intensity"] = this.Intensity;

            //shader["lightView"] = this.LightMatrix;
            //shader["tShadowMap"] = this.Depth;
        }
    }
}
