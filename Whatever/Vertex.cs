﻿using OpenTK;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL4;

namespace Whatever {
    public struct Vertex {
        public const int Size = (3 + 4 + 2 + 3 + 3 + 3) * 4;

        private readonly Vector3 position;
        private readonly Color4 color;
        private readonly Vector2 texcoord;
        private readonly Vector3 normal;
        private readonly Vector3 tangent;
        private readonly Vector3 biTangent;

        public Vertex (Vector3 pos, Color4 color, Vector2 texcoord, Vector3 normal, Vector3 tangent) {
            this.position = pos;
            this.color = color;
            this.texcoord = texcoord;
            this.normal = normal;
            this.tangent = tangent;
            this.biTangent = -Vector3.Cross (normal, tangent).Normalized ();
        }

        public static void EnableAttribs (int array) {
            GL.VertexArrayAttribBinding (array, 0, 0);
            GL.EnableVertexArrayAttrib (array, 0);
            GL.VertexArrayAttribFormat (array, 0, 3, VertexAttribType.Float, false, 0);
            
            GL.VertexArrayAttribBinding (array, 1, 0);
            GL.EnableVertexArrayAttrib (array, 1);
            GL.VertexArrayAttribFormat (array, 1, 4, VertexAttribType.Float, false, Vector3.SizeInBytes);

            GL.VertexArrayAttribBinding (array, 2, 0);
            GL.EnableVertexArrayAttrib (array, 2);
            GL.VertexArrayAttribFormat (array, 2, 2, VertexAttribType.Float, false, Vector3.SizeInBytes + Vector4.SizeInBytes);

            GL.VertexArrayAttribBinding (array, 3, 0);
            GL.EnableVertexArrayAttrib (array, 3);
            GL.VertexArrayAttribFormat (array, 3, 3, VertexAttribType.Float, false, Vector3.SizeInBytes + Vector4.SizeInBytes + Vector2.SizeInBytes);

            GL.VertexArrayAttribBinding (array, 4, 0);
            GL.EnableVertexArrayAttrib (array, 4);
            GL.VertexArrayAttribFormat (array, 4, 3, VertexAttribType.Float, false, Vector3.SizeInBytes + Vector4.SizeInBytes + Vector2.SizeInBytes + Vector3.SizeInBytes);

            GL.VertexArrayAttribBinding (array, 5, 0);
            GL.EnableVertexArrayAttrib (array, 5);
            GL.VertexArrayAttribFormat (array, 5, 3, VertexAttribType.Float, false, Vector3.SizeInBytes + Vector4.SizeInBytes + Vector2.SizeInBytes + Vector3.SizeInBytes + Vector3.SizeInBytes);
        }
    }
}
