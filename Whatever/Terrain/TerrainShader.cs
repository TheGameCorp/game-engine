﻿using OpenTK.Graphics.OpenGL4;
using Whatever.Shaders;

namespace Whatever.Terrain {
    class TerrainShader : Shader {
        protected override string ShaderName => "Terrain";

        public TerrainShader () {
            this.Shaders.Add (ShaderType.VertexShader);
            this.Shaders.Add (ShaderType.TessControlShader);
            this.Shaders.Add (ShaderType.TessEvaluationShader);
            this.Shaders.Add (ShaderType.GeometryShader);
            this.Shaders.Add (ShaderType.FragmentShader);

            this.CreateProgram ();
        }

        public override void SetUniforms (Transform transform) { }
    }
}
