﻿using System.Collections.Generic;
using System.Linq;
using OpenTK;
using OpenTK.Graphics;

namespace Whatever.Terrain {
    public class Terrain : RenderableObject {
        private TerrainConfig Config;

        public static float OneO3 = 1f / 3f;
        public static float OneOH = 1f / 1.5f;

        public Terrain (TerrainConfig config) {
            this.Config = config;
            this.Material = ("crate0_diffuse.png", "crate0_normal.png");

            List<Vertex> verts = new List<Vertex> ();
            List<uint> inds = new List<uint> ();

            foreach (var v in this.GeneratePatch ())
                verts.Add(new Vertex (v, Color4.White, new Vector2 (0, 0), new Vector3 (0, 1, 0), new Vector3 (0, 1, 0)));

            foreach (var v in (new List<uint> { 0, 1, 2, 3, 4, 5 }).Permute ()) {
                foreach (var vv in v)
                    inds.Add (vv);
            }

            this.Geometry = (
                verts.ToArray ()
            ,
                inds.ToArray ()
            );
        }

        public Vector3[] GeneratePatch () => new Vector3[16] {
            new Vector3 (0, 0, 0),
            new Vector3 (OneO3, 0, 0),
            new Vector3 (OneOH, 0, 0),
            new Vector3 (1, 0, 0),

            new Vector3 (0, 0, OneO3),
            new Vector3 (OneO3, 0, OneO3),
            new Vector3 (OneOH, 0, OneO3),
            new Vector3 (1, 0, OneO3),

            new Vector3 (0, 0, OneOH),
            new Vector3 (OneO3, 0, OneOH),
            new Vector3 (OneOH, 0, OneOH),
            new Vector3 (1, 0, OneOH),

            new Vector3 (0, 0, 1),
            new Vector3 (OneO3, 0, 1),
            new Vector3 (OneOH, 0, 1),
            new Vector3 (1, 0, 1),
        };

        public void UpdateQuadtree () {
            if (Window.Camera.Moved) {
                //((TerrainQuadtree) this.Children[0]).UpdateQuadtree ();
            }
        }
    }

    public static class TerrainExtensions {
        public static IEnumerable<IEnumerable<T>> Permute<T> (this IEnumerable<T> sequence) {
            if (sequence == null) {
                yield break;
            }

            var list = sequence.ToList ();

            if (!list.Any ()) {
                yield return Enumerable.Empty<T> ();
            } else {
                var startingElementIndex = 0;

                foreach (var startingElement in list) {
                    var remainingItems = list.AllExcept (startingElementIndex);

                    foreach (var permutationOfRemainder in remainingItems.Permute ()) {
                        yield return startingElement.Concat (permutationOfRemainder);
                    }

                    startingElementIndex++;
                }
            }
        }

        private static IEnumerable<T> Concat<T> (this T firstElement, IEnumerable<T> secondSequence) {
            yield return firstElement;
            if (secondSequence == null) {
                yield break;
            }

            foreach (var item in secondSequence) {
                yield return item;
            }
        }

        private static IEnumerable<T> AllExcept<T> (this IEnumerable<T> sequence, int indexToSkip) {
            if (sequence == null) {
                yield break;
            }

            var index = 0;

            foreach (var item in sequence.Where (item => index++ != indexToSkip)) {
                yield return item;
            }
        }
    }
}
