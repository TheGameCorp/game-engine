﻿using System;
using System.ComponentModel;
using Newtonsoft.Json;

namespace Whatever.Terrain {
    public class TerrainConfig {
        public float ScaleY { get; set; }
        public float ScaleXZ { get; set; }

        public int[] LodRange { get; set; } = new int[8];
        [JsonIgnore]
        public int[] LodMorphingArea { get; set; } = new int[8];

        private int updateMorphingAreas (int lod) => (int) ((this.ScaleXZ / TerrainQuadtree.RootNodes) / Math.Pow (2, lod));

        private void setLodRange (int index, int lodRange) {
            this.LodRange[index] = lodRange;
            this.LodMorphingArea[index] = lodRange - this.updateMorphingAreas (index + 1);
        }

        public TerrainConfig () { }

        [JsonConstructor]
        public TerrainConfig (float scaleY, float scaleXZ, int[] lodRange) {
            this.ScaleY = scaleY;
            this.ScaleXZ = scaleXZ;
            for (int i = 0; i < lodRange.Length; i++)
                this.setLodRange (i, lodRange[i]);
        }

        public static implicit operator TerrainConfig (string @this) => Config.LoadConfig<TerrainConfig> (@this);
    }
}
