﻿using System;
using OpenTK;
using Whatever.Shaders;

namespace Whatever.Terrain {
    public class TerrainNode : SceneObject {
        private bool isLeaf;
        private TerrainConfig config;
        private int lod;
        private Vector2 location;
        private Vector2 worldPos;
        private Vector2 index;
        private float gap;
        private PatchBuffer buffer;

        public TerrainNode (PatchBuffer buffer, TerrainConfig config, Vector2 location, int lod, Vector2 index) {
            this.buffer = buffer;
            this.config = config;
            this.location = location;
            this.lod = lod;
            this.index = index;
            this.gap = 1f / (TerrainQuadtree.RootNodes * (float) Math.Pow (2, lod));

            var scaling = new Vector3 (this.gap, 0, this.gap);
            var trans = new Vector3 (this.location.X, 0, this.location.Y);

            this.LocalTransform.Scale = scaling;
            this.LocalTransform.Position = trans;

            this.WorldTransform.Scale = new Vector3 (this.config.ScaleXZ, this.config.ScaleY, this.config.ScaleXZ);
            this.WorldTransform.Position = new Vector3 (-this.config.ScaleXZ / 2f, 0, -this.config.ScaleXZ / 2f);

            this.computeWorldPos ();
            this.UpdateQuadtree ();
        }

        public void Render (Shader shader) {
            if (this.isLeaf)
                this.render (shader);

            foreach (var child in this.Children)
                switch (child) {
                    case TerrainNode n:
                        n.Render (shader);
                        break;
                }
        }

        public void UpdateQuadtree (){
            if (Window.Camera.Position.Y > this.config.ScaleY)
                this.worldPos.Y = this.config.ScaleY;
            else
                this.worldPos.Y = Window.Camera.Position.Y;

            this.updateChildNodes ();

            foreach (var child in this.Children)
                switch (child) {
                    case TerrainNode n:
                        n.UpdateQuadtree ();
                        break;
                }
        }

        private void addChildNodes (int lod) {
            if (this.isLeaf)
                this.isLeaf = false;

            if (this.Children.Count == 0)
                for (int x = 0; x < TerrainQuadtree.RootNodes; x++) {
                    for (int y = 0; y < TerrainQuadtree.RootNodes; y++)
                        this.AddChild (new TerrainNode (this.buffer, this.config, this.location + new Vector2 ((float) x * this.gap / 2f, (float) y * this.gap / 2f), lod, new Vector2 (x, y)));
                }
        }

        private void removeChildNodes () {
            if (!this.isLeaf)
                this.isLeaf = true;

            if (this.Children.Count != 0)
                this.Children.Clear ();
        }

        private void updateChildNodes () {
            float dist = (Window.Camera.Position - new Vector3(this.worldPos.X, 0, this.worldPos.Y)).Length;

            if (dist < this.config.LodRange[this.lod])
                this.addChildNodes (this.lod + 1);
            else if (dist >= this.config.LodRange[this.lod])
                this.removeChildNodes ();
        }

        public void computeWorldPos () {
            var loc = this.location + new Vector2 (this.gap / 2f) * new Vector2 (this.config.ScaleXZ) - new Vector2 (this.config.ScaleXZ / 2f);

            this.worldPos = new Vector2 (loc.X, loc.Y);
        }

        private void render (Shader shader) {
            shader["localMatrix"] = this.LocalTransform;
            shader["worldMatrix"] = this.WorldTransform;
            shader["camera"] = Window.Camera.LookAtMatrix;

            shader.Use (() => {
                this.buffer.Draw ();
                Window.DRAWCALLS++;
            });
        }
    }
}
