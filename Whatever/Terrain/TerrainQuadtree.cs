﻿using OpenTK;
using Whatever.Shaders;

namespace Whatever.Terrain {
    public class TerrainQuadtree : RenderableObject {
        public static int RootNodes = 8;

        public static float OneO3 = 1f / 3f;
        public static float OneOH = 1f / 1.5f;

        public TerrainQuadtree (TerrainConfig config) : base () {
            PatchBuffer buffer = new PatchBuffer ();
            buffer.Allocate (this.GeneratePatch ());

            for (int x = 0; x < TerrainQuadtree.RootNodes; x++) {
                for (int y = 0; y < TerrainQuadtree.RootNodes; y++)
                    this.AddChild (new TerrainNode (buffer, config, new Vector2 ((float) x / (float) TerrainQuadtree.RootNodes, (float) y / (float) TerrainQuadtree.RootNodes), 0, new Vector2 (x, y)));
            }

            this.WorldTransform.Scale = new Vector3 (config.ScaleXZ, config.ScaleY, config.ScaleXZ);
            this.WorldTransform.Position = new Vector3 (config.ScaleXZ / 2f, 0, config.ScaleXZ / 2f);
        }

        public void UpdateQuadtree () {
            foreach (var child in this.Children) {
                ((TerrainNode) child).UpdateQuadtree ();
            }
        }

        new public void Render (Shader shader) => ((TerrainNode) this.Children[0]).Render (shader);

        public Vector2[] GeneratePatch () => new Vector2[16] {
            new Vector2 (0, 0),
            new Vector2 (OneO3, 0),
            new Vector2 (OneOH, 0),
            new Vector2 (1, 0),

            new Vector2 (0, OneO3),
            new Vector2 (OneO3, OneO3),
            new Vector2 (OneOH, OneO3),
            new Vector2 (1, OneO3),

            new Vector2 (0, OneOH),
            new Vector2 (OneO3, OneOH),
            new Vector2 (OneOH, OneOH),
            new Vector2 (1, OneOH),

            new Vector2 (0, 1),
            new Vector2 (OneO3, 1),
            new Vector2 (OneOH, 1),
            new Vector2 (1, 1),
        };
    }
}
