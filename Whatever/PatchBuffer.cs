﻿using OpenTK;
using OpenTK.Graphics.OpenGL4;

namespace Whatever {
    public class PatchBuffer {
        private int vboId;
        private int vaoId;
        private int size;

        public PatchBuffer () {
            this.vboId = GL.GenBuffer ();
            this.vaoId = GL.GenVertexArray ();
        }

        public void Allocate (Vector2[] verts) {
            this.size = verts.Length;

            GL.BindVertexArray (this.vaoId);
            GL.BindBuffer (BufferTarget.ArrayBuffer, this.vboId);

            GL.BufferData (BufferTarget.ArrayBuffer, this.size * Vector2.SizeInBytes, verts, BufferUsageHint.StaticDraw);

            GL.VertexAttribPointer (0, 2, VertexAttribPointerType.Float, false, sizeof (float) * 2, 0);
            GL.PatchParameter (PatchParameterInt.PatchVertices, this.size);
        }

        public void Draw () {
            GL.BindVertexArray (this.vaoId);
            GL.EnableVertexAttribArray (0);

            GL.DrawArrays (PrimitiveType.Patches, 0, this.size);
        }
    }
}
