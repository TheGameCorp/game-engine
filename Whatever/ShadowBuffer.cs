﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whatever {
    public class ShadowBuffer : Framebuffer {
        public ShadowBuffer (int width, int height) : base (width, height, true) {
        }
    }
}
