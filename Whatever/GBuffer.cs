﻿using OpenTK.Graphics.OpenGL4;

namespace Whatever {
    public class GBuffer : Framebuffer {
        public Texture Normal;
        public Texture MetRoughAO;

        public GBuffer (int width, int height) : base (width, height) {
            this.Normal = new Texture (this.Size.width, this.Size.height, Texture.TextureFilter.Linear);
            this.MetRoughAO = new Texture (this.Size.width, this.Size.height, Texture.TextureFilter.Linear);

            GL.NamedFramebufferTexture (this.FramebufferID, FramebufferAttachment.ColorAttachment1, this.Normal.TextureID, 0);
            GL.NamedFramebufferTexture (this.FramebufferID, FramebufferAttachment.ColorAttachment2, this.MetRoughAO.TextureID, 0);

            GL.NamedFramebufferDrawBuffers (this.FramebufferID, 3, new DrawBuffersEnum[] { DrawBuffersEnum.ColorAttachment0, DrawBuffersEnum.ColorAttachment1, DrawBuffersEnum.ColorAttachment2 });
        }
    }
}
