﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using OpenTK;
using OpenTK.Graphics.OpenGL4;
using Whatever.Shaders;

namespace Whatever {
    public class Geometry : IDisposable {
        public int VertexCount { get; private set; }
        private int VertexArray;
        private int[] Buffers;
        private int[] IndexCount { get; set; }

        public Geometry (Vertex[] vertices) {
            this.VertexCount = vertices.Length;

            GL.CreateVertexArrays (1, out this.VertexArray);

            this.Buffers = new int[1];
            GL.CreateBuffers (1, this.Buffers);

            GL.NamedBufferStorage (this.Buffers[0], Vertex.Size * this.VertexCount, vertices, BufferStorageFlags.MapWriteBit);

            GL.VertexArrayVertexBuffer (this.VertexArray, 0, this.Buffers[0], IntPtr.Zero, Vertex.Size);
            Vertex.EnableAttribs (this.VertexArray);

            this.initialized = true;
        }

        public Geometry (Vertex[] vertices, uint[] indices) {
            this.VertexCount = vertices.Length;
            this.IndexCount = new int[] { indices.Length };

            GL.CreateVertexArrays (1, out this.VertexArray);

            this.Buffers = new int[2];
            GL.CreateBuffers (2, this.Buffers);

            GL.NamedBufferStorage (this.Buffers[0], Vertex.Size * this.VertexCount, vertices, BufferStorageFlags.MapWriteBit);
            GL.NamedBufferStorage (this.Buffers[1], sizeof (uint) * this.IndexCount[0], indices, BufferStorageFlags.MapWriteBit);

            GL.VertexArrayVertexBuffer (this.VertexArray, 0, this.Buffers[0], IntPtr.Zero, Vertex.Size);
            Vertex.EnableAttribs (this.VertexArray);

            this.initialized = true;
        }

        public Geometry (Vertex[] vertices, List<uint[]> indices) {
            this.VertexCount = vertices.Length;

            var tempLen = new List<int> ();
            foreach (var v in indices)
                tempLen.Add (v.Length);
            this.IndexCount = tempLen.ToArray ();

            GL.CreateVertexArrays (1, out this.VertexArray);

            this.Buffers = new int[indices.Count + 1];
            GL.CreateBuffers (indices.Count + 1, this.Buffers);

            GL.NamedBufferStorage (this.Buffers[0], Vertex.Size * this.VertexCount, vertices, BufferStorageFlags.MapWriteBit);

            for (int i = 0; i < indices.Count; i++) {
                GL.NamedBufferStorage (this.Buffers[i + 1], sizeof (uint) * indices[i].Length, indices[i], BufferStorageFlags.MapWriteBit);
            }

            GL.VertexArrayVertexBuffer (this.VertexArray, 0, this.Buffers[0], IntPtr.Zero, Vertex.Size);
            Vertex.EnableAttribs (this.VertexArray);

            this.initialized = true;
        }

        public void Render (Shader shader, Material material, Transform transform) {
            //shader["camPos"] = Window.Camera.Position;

            shader.SetUniforms (transform);

            material.Use (shader, () => {
                this.RenderCall ();
            });
        }

        public void Render (BaseLight light, GeometryShader shader, Material material, Transform transform) {
            //shader["camPos"] = Window.Camera.Position;

            switch (light) {
                case DirectionalLight dir:
                    shader.SetUniforms (transform, dir);
                    break;
            }

            material.Use (shader, () => {
                this.RenderCall ();
            });
        }

        public void RenderDebug (Shader shader, Transform transform) {
            shader["normal_length"] = 0.1f;
            shader.SetUniforms (transform);
            
            this.RenderCall ();
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        public void RenderCall () {
            GL.BindVertexArray (this.VertexArray);
            if (this.initialized)
                if (this.Buffers.Length == 1) {
                    GL.DrawArrays (PrimitiveType.Triangles, 0, this.VertexCount);
                    Window.DRAWCALLS++;
                } else {
                    for (int i = 1; i < this.Buffers.Length; i++) {
                        GL.VertexArrayElementBuffer (this.VertexArray, this.Buffers[i]);
                        GL.DrawElements (BeginMode.Triangles, this.IndexCount[i - 1], DrawElementsType.UnsignedInt, 0);
                        Window.DRAWCALLS++;
                    }
                }
        }

        public static implicit operator Geometry (Vertex[] vertices) => new Geometry (vertices);
        public static implicit operator Geometry ((Vertex[] vertices, uint[] indices) geom) => new Geometry (geom.vertices, geom.indices);
        public static implicit operator Geometry ((Vertex[] vertices, List<uint[]> ranges) geom) => new Geometry (geom.vertices, geom.ranges);
        public static implicit operator Geometry (string model) => AssimpLoader.LoadModel (model);

        #region IDisposable Support
        private bool initialized = false;

        protected virtual void Dispose (bool disposing) {
            if (disposing) {
                if (this.initialized) {
                    GL.DeleteVertexArray (this.VertexArray);
                    if (this.Buffers[1] != -1)
                        GL.DeleteBuffers (2, this.Buffers);
                    else
                        GL.DeleteBuffers (1, this.Buffers);

                    this.initialized = false;
                }
            }
        }
        public void Dispose () {
            Dispose (true);
            GC.SuppressFinalize (this);
        }
        #endregion
    }
}
