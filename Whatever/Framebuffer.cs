﻿using System;
using OpenTK.Graphics;
using OpenTK.Graphics.OpenGL;

namespace Whatever {
    public class Framebuffer : IDisposable {
        public int FramebufferID = -1;
        public Texture Color;
        public Texture Depth;

        public bool DepthOnly { get; }

        public (int width, int height) Size { get; }

        public Framebuffer (int width, int height, bool depthOnly = false) {
            this.Size = (width, height);
            this.DepthOnly = depthOnly;
            if (!this.DepthOnly)
                this.Color = new Texture (this.Size.width, this.Size.height, Texture.TextureFilter.Linear);
            this.Depth = new Texture (this.Size.width, this.Size.height, PixelInternalFormat.DepthComponent32f, PixelFormat.DepthComponent, Texture.TextureFilter.Linear);

            GL.CreateFramebuffers (1, out this.FramebufferID);

            if (!this.DepthOnly)
                GL.NamedFramebufferTexture (this.FramebufferID, FramebufferAttachment.ColorAttachment0, this.Color.TextureID, 0);
            GL.NamedFramebufferTexture (this.FramebufferID, FramebufferAttachment.DepthAttachment, this.Depth.TextureID, 0);

            if (!this.DepthOnly)
                GL.NamedFramebufferDrawBuffers (this.FramebufferID, 1, new DrawBuffersEnum[] { DrawBuffersEnum.ColorAttachment0 });
        }

        public void Use (Action action) => action ();

        public void Begin () {
            GL.BindFramebuffer (FramebufferTarget.Framebuffer, this.FramebufferID);
            GL.ClearColor (0, 0, 0, 0);
            GL.Clear (!this.DepthOnly ? ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit : ClearBufferMask.DepthBufferBit);
            GL.Viewport (0, 0, this.Size.width, this.Size.height);
        }

        public void End () {
            GL.BindFramebuffer (FramebufferTarget.Framebuffer, 0);
            GL.DrawBuffer (DrawBufferMode.Back);
        }

        public void Dispose () {
            this.Color.Dispose ();
            this.Depth.Dispose ();
            GL.DeleteFramebuffer (this.FramebufferID);
        }
    }


    [Serializable]
    public class FramebufferException : Exception {
        public FramebufferException () { }
        public FramebufferException (string message) : base (message) { }
        public FramebufferException (string message, Exception inner) : base (message, inner) { }
        protected FramebufferException (
          System.Runtime.Serialization.SerializationInfo info,
          System.Runtime.Serialization.StreamingContext context) : base (info, context) { }
    }
}
