﻿using OpenTK;
using OpenTK.Input;

namespace Whatever {
    public interface ICamera {
        Matrix4 Projection { get; }
        Matrix4 LookAtMatrix { get; }
        Matrix4 View { get; }
        Vector3 Position { get; }
        Vector3 Rotation { get; }
        float AspectRatio { set; }
        bool Moved { get; }
        void Update (KeyboardState keys, float dt);
        void UpdateAim (float dx, float dy, float dt);

        void Translate (Vector3 pos);
    }
}
